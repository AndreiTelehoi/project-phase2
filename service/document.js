const { Document } = require("../models/document")
const { DocumentBind } = require("../models/documentBind")

const document = {
	getByUserId: async userId => {
		return await Document.findAll({
			where: { userId: parseInt(userId) }
		}).then(documents => {
			return documents
		})
	},
	getShared: async userId => {
		return await DocumentBind.findAll({
			where: { userId: parseInt(userId) }
		}).then(async documentsBinds => {
			return await Document.findAll({
				where: { id: documentsBinds.map(bind => bind.documentId) }
			}).then(documents => {
				return documents
			})
		})
	},
	create: async document => {
		try {
			return await Document.create(document)
		} catch (err) {
			throw new Error(err.message)
		}
	},
	modify: async (documentId, document) => {
		try {
			return Document.update(document, { where: { id: parseInt(documentId) } })
		} catch (err) {
			throw new Error(err.message)
		}
	},
	delete: async documentId => {
		try {
			await DocumentBind.destroy({
				where: { documentId: parseInt(documentId) }
			})
			return await Document.destroy({
				where: { id: parseInt(documentId) }
			})
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = document
