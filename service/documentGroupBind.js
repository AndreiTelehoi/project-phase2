const { DocumentGroupBind } = require("../models/documentGroupBind")
const { Document } = require("../models/document")

const documentGroupBind = {
	getGroupSharedDocuments: async groupId => {
		return await DocumentGroupBind.findAll({
			where: { groupId: parseInt(groupId) }
		}).then(async documentsBinds => {
			return await Document.findAll({
				where: {
					id: documentsBinds.map(documentBind => documentBind.documentId)
				}
			})
		})
	},

	create: async documentGroupBind => {
		try {
			return await DocumentGroupBind.create(documentGroupBind)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = documentGroupBind
