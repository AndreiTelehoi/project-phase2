const { Subject } = require("../models/subject")

const subject = {
	get: async () => {
		return await Subject.findAll().then(subjects => {
			return subjects
		})
	}
}

module.exports = subject
