const { DocumentBind } = require("../models/documentBind")

const documentBind = {
	create: async documentBind => {
		try {
			return await DocumentBind.create(documentBind)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = documentBind
