const { Group } = require("../models/group")
const { GroupBind } = require("../models/groupBind")

const group = {
	getByUserId: async userId => {
		return await GroupBind.findAll({
			where: { userId: parseInt(userId) }
		}).then(async groupsBinds => {
			return await Group.findAll({
				where: { id: groupsBinds.map(groupBind => groupBind.groupId) }
			}).then(async sharedGroups => {
				return await Group.findAll({
					where: { userId: userId }
				}).then(myGroups => {
					return {
						myGroups,
						sharedGroups
					}
				})
			})
		})
	},
	create: async group => {
		try {
			return await Group.create(group)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = group
