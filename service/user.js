const { User } = require("../models/user")

const user = {
	getByEmail: async email => {
		return await User.findOne({
			where: { email: email }
		}).then(user => {
			return user
		})
	},

	getUsers: async () => {
		return await User.findAll({ attributes: ["email", "id"] }).then(users => {
			return users
		})
	}
}

module.exports = user
