const { GroupBind } = require("../models/groupBind")

const groupBind = {
	create: async groupBind => {
		try {
			return await GroupBind.create(groupBind)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = groupBind
