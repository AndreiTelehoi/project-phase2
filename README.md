# project-phase2

Team Name: WebDevs

Team Topic: Web app for taking notes during courses/labs (5)

Team Members:

- Telehoi George-Andrei - Product Owner
- Tudosie Raluca - Developer
- Tutuluș Andreea - Developer
- Stoica Alexandra - Developer
- Tîrlescu Roxana Ștefania - Project Manager