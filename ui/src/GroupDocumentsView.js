import React from "react"
import DocumentList from "./DocumentList"

import axios from "axios"

class GroupDocumentsView extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			documents: [],
			userId: undefined
		}

		this.change = this.change.bind(this)
		this.submit = this.submit.bind(this)
	}

	async submit() {
		await axios.post(`http://localhost:8080/api/groupBind`, {
			userId: this.state.userId,
			groupId: this.props.group.id
		})
		this.setState({ userId: undefined })
	}

	change(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	componentDidMount() {
		axios
			.get(`http://localhost:8080/api/groupSharedDocument/${this.props.group.id}`)
			.then(response => {
				this.setState({
					documents: response.data
				})
			})
	}

	render() {
		return (
			<div className='ml-3 mt-3'>
				<h2>{this.props.group.name}</h2>
				<br></br>
				<h3>Shared documents</h3>
				<DocumentList
					documents={this.state.documents}
					subjects={this.props.subjects}
					filter={() => ""}
				></DocumentList>
				<h2>Share group</h2>
				<div className='row ml-3'>
					<form>
						<select
							value={this.state.userId}
							className='form-control'
							name='userId'
							onChange={this.change}
						>
							{this.props.users.map(user => (
								<option value={user.id}>{user.email}</option>
							))}
						</select>
						<button type='button' className='btn btn-xs btn-success mt-1' onClick={this.submit}>
							Add user
						</button>
						<button
							className='btn btn-xs btn-warning mt-1 ml-1'
							onClick={this.props.showGroupDetailsFalse}
						>
							Back
						</button>
					</form>
				</div>
			</div>
		)
	}
}

export default GroupDocumentsView
