import React from "react"

import DocumentList from "./DocumentList"
import GroupForm from "./GroupForm"
import GroupList from "./GroupList"
import DocumentForm from "./DocumentForm"

import axios from "axios"

class DocumentsView extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			myDocuments: [],
			sharedDocuments: [],
			myGroups: [],
			sharedGroups: [],
			users: []
		}

		this.deleteDocument = this.deleteDocument.bind(this)
		this.getDocuments = this.getDocuments.bind(this)
		this.getGroups = this.getGroups.bind(this)
		this.filter = this.filter.bind(this)
	}
	filter(criteria) {
		this.setState(prevState => {
			let array = [...prevState.myDocuments]
			array.sort((a, b) => (a[criteria] > b[criteria] ? 1 : -1))
			return {
				myDocuments: array
			}
		})
	}

	getGroups() {
		axios.get(`http://localhost:8080/api/group/${this.props.user.id}`).then(response => {
			this.setState({
				myGroups: response.data.myGroups,
				sharedGroups: response.data.sharedGroups
			})
		})
	}

	getDocuments() {
		axios.get(`http://localhost:8080/api/document/${this.props.user.id}`).then(response => {
			this.setState({
				myDocuments: response.data
			})
		})
	}

	async deleteDocument(documentId) {
		await axios.delete(`http://localhost:8080/api/document/${documentId}`)
		this.getDocuments()
	}

	componentDidMount() {
		this.getDocuments()
		this.getGroups()
		axios.get(`http://localhost:8080/api/sharedDocument/${this.props.user.id}`).then(response => {
			this.setState({
				sharedDocuments: response.data
			})
		})
	}

	render() {
		return (
			<div className='ml-5 mt-3'>
				<h3>My documents</h3>
				<div className='row'>
					<DocumentList
						documents={this.state.myDocuments}
						subjects={this.props.subjects}
						deleteDocument={this.deleteDocument}
						groups={this.state.myGroups}
						users={this.props.users}
						getDocuments={this.getDocuments}
						filter={this.filter}
					></DocumentList>
				</div>
				<h3>Shared documents</h3>
				<div className='row'>
					<DocumentList
						documents={this.state.sharedDocuments}
						subjects={this.props.subjects}
						filter={() => ""}
					></DocumentList>
				</div>
				<h3>Add new document</h3>
				<div className='row'>
					<DocumentForm
						subjects={this.props.subjects}
						getDocuments={this.getDocuments}
						user={this.props.user}
					></DocumentForm>
				</div>
				<h3>Groups list</h3>
				<div className='row'>
					<GroupList
						showGroupDetailsTrue={this.props.showGroupDetailsTrue}
						groups={this.state.myGroups}
						admin={true}
					></GroupList>
				</div>
				<h3>Shared groups</h3>
				<div className='row'>
					<GroupList
						showGroupDetailsTrue={this.props.showGroupDetailsTrue}
						groups={this.state.sharedGroups}
					></GroupList>
				</div>
				<h3>Add new group</h3>
				<div className='row'>
					<GroupForm getGroups={this.getGroups} user={this.props.user}></GroupForm>
				</div>
			</div>
		)
	}
}

export default DocumentsView
