import React from "react"

class GroupList extends React.Component {
	state = {}
	render() {
		return (
			<table className='table col-sm-10'>
				<thead>
					<tr>
						<th>Name</th>
						<th>Details</th>
					</tr>
				</thead>
				<tbody>
					{this.props.groups.map(group => (
						<tr key={group.id}>
							<td>{group.name}</td>
							<td>
								<button
									className='btn btn-xs btn-primary'
									onClick={() => this.props.showGroupDetailsTrue(group)}
								>
									Details
								</button>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		)
	}
}

export default GroupList
