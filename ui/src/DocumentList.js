import React from "react"
import Modal from "react-modal"

import axios from "axios"

class DocumentList extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			shareRequest: false,
			selectedUser: undefined,
			selectedGroup: undefined,
			selectedDocument: undefined,
			editing: false,
			documentId: "",
			name: "",
			content: "",
			tag: "",
			subjectId: ""
		}

		this.getSubjectName = this.getSubjectName.bind(this)
		this.requestShare = this.requestShare.bind(this)
		this.cancelShareRequest = this.cancelShareRequest.bind(this)
		this.shareWithGroup = this.shareWithGroup.bind(this)
		this.shareWithUser = this.shareWithUser.bind(this)
		this.change = this.change.bind(this)
		this.triggerEdit = this.triggerEdit.bind(this)
		this.cancelEdit = this.cancelEdit.bind(this)
		this.modify = this.modify.bind(this)
	}

	async modify() {
		await axios.put(`http://localhost:8080/api/document/${this.state.documentId}`, {
			name: this.state.name,
			content: this.state.content,
			tag: this.state.tag,
			subjectId: this.state.subjectId
		})
		this.props.getDocuments()
		this.cancelEdit()
	}

	triggerEdit(document) {
		this.setState({
			editing: true,
			name: document.name,
			content: document.content,
			tag: document.tag,
			subjectId: document.subjectId,
			documentId: document.id
		})
	}

	cancelEdit() {
		this.setState({ editing: false, name: "", content: "", tag: "", subjectId: "", documentId: "" })
	}

	change(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async shareWithGroup(event) {
		event.preventDefault()
		await axios.post("http://localhost:8080/api/documentGroupBind", {
			documentId: this.state.selectedDocument,
			groupId: this.state.selectedGroup
		})
		this.cancelShareRequest()
	}

	async shareWithUser(event) {
		event.preventDefault()
		await axios.post("http://localhost:8080/api/documentBind", {
			documentId: this.state.selectedDocument,
			userId: this.state.selectedUser
		})
		this.cancelShareRequest()
	}

	requestShare(documentId) {
		this.setState({ shareRequest: true, selectedDocument: documentId })
	}
	cancelShareRequest() {
		this.setState({
			shareRequest: false,
			selectedGroup: undefined,
			selectedUser: undefined,
			selectedDocument: undefined
		})
	}

	getSubjectName(document) {
		const subject = this.props.subjects.find(subject => subject.id === document.subjectId)
		return subject && subject.name
	}

	render() {
		return (
			<div>
				<table className='table col-sm-10'>
					<thead>
						<tr>
							<th onClick={() => this.props.filter("name")}>Name</th>
							<th onClick={() => this.props.filter("content")}>Content</th>
							<th onClick={() => this.props.filter("data")}>Data</th>
							<th onClick={() => this.props.filter("tag")}>Tag</th>
							<th>Subject</th>
							{this.props.deleteDocument ? (
								<>
									<th>Edit</th>
									<th>Delete</th>
									<th>Share</th>
								</>
							) : null}
						</tr>
					</thead>
					<tbody>
						{this.props.documents.map(document => (
							<tr key={document.id}>
								{this.state.editing && this.state.documentId === document.id ? (
									<>
										<td>
											<input
												value={this.state.name}
												className='form-control'
												type='text'
												name='name'
												onChange={this.change}
											/>
										</td>
										<td>
											<input
												value={this.state.content}
												className='form-control'
												type='text'
												name='content'
												onChange={this.change}
											/>
										</td>
										<td>{new Date(document.data).toLocaleDateString()}</td>
										<td>
											<input
												value={this.state.tag}
												className='form-control'
												type='text'
												name='tag'
												onChange={this.change}
											/>
										</td>
										<td>
											<select
												value={this.state.subjectId}
												className='form-control'
												name='subjectId'
												onChange={this.change}
											>
												{this.props.subjects.map(subject => (
													<option value={subject.id}>{subject.name}</option>
												))}
											</select>
										</td>
										<td>
											<button className='btn btn-xs btn-primary' onClick={this.modify}>
												Ok
											</button>
										</td>
										<td>
											<button className='btn btn-xs btn-danger' onClick={this.cancelEdit}>
												Cancel
											</button>
										</td>
									</>
								) : (
									<>
										<td>{document.name}</td>
										<td>{document.content}</td>
										<td>{new Date(document.data).toLocaleDateString()}</td>
										<td>{document.tag}</td>
										<td>{this.getSubjectName(document)}</td>
										{this.props.deleteDocument ? (
											<>
												<td>
													<button
														className='btn btn-xs btn-primary'
														onClick={() => this.triggerEdit(document)}
													>
														Edit
													</button>
												</td>
												<td>
													<button
														className='btn btn-xs btn-danger'
														onClick={() => this.props.deleteDocument(document.id)}
													>
														Delete
													</button>
												</td>
											</>
										) : null}
									</>
								)}
								{this.props.deleteDocument ? (
									<td>
										<button
											className='btn btn-xs btn-success'
											onClick={() => this.requestShare(document.id)}
										>
											Share
										</button>
									</td>
								) : null}
							</tr>
						))}
					</tbody>
				</table>
				{this.props.deleteDocument ? (
					<Modal
						isOpen={this.state.shareRequest}
						onRequestClose={this.setArataModalFalse}
						style={{
							content: {
								top: "50%",
								left: "50%",
								right: "auto",
								bottom: "auto",
								marginRight: "-50%",
								transform: "translate(-50%, -50%)"
							}
						}}
						contentLabel='shareDialog'
					>
						<h4>Share with user</h4>
						<form>
							<select
								value={this.state.selectedUser}
								className='form-control'
								name='selectedUser'
								onChange={this.change}
							>
								{this.props.users.map(user => (
									<option value={user.id}>{user.email}</option>
								))}
							</select>
							<button className='btn btn-sm btn-primary mt-2' onClick={this.shareWithUser}>
								Share
							</button>
						</form>
						<h4>Share with group</h4>
						<form>
							<select
								value={this.state.selectedGroup}
								className='form-control'
								name='selectedGroup'
								onChange={this.change}
							>
								{this.props.groups.map(group => (
									<option value={group.id}>{group.name}</option>
								))}
							</select>
							<button className='btn btn-sm btn-primary mt-2' onClick={this.shareWithGroup}>
								Share
							</button>
						</form>
						<button
							className='btn btn-sm btn-warning float-right'
							onClick={this.cancelShareRequest}
						>
							Close
						</button>
					</Modal>
				) : null}
			</div>
		)
	}
}

export default DocumentList
