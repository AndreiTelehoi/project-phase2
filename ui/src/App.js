import React from "react"
import "./App.css"

import LoginView from "./LoginView"
import DocumentsView from "./DocumentsView"
import GroupDocumentsView from "./GroupDocumentsView"

import axios from "axios"

class App extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			isLoggedIn: false,
			user: undefined,
			showGroupDetails: false,
			group: undefined,
			subjects: [],
			users: []
		}

		this.login = this.login.bind(this)
		this.showGroupDetailsFalse = this.showGroupDetailsFalse.bind(this)
		this.showGroupDetailsTrue = this.showGroupDetailsTrue.bind(this)
	}

	showGroupDetailsTrue(group) {
		this.setState({ showGroupDetails: true, group: group })
	}

	showGroupDetailsFalse() {
		this.setState({ showGroupDetails: false, group: undefined })
	}

	login(eMail, password) {
		// axios.get(`http://localhost:8080/api/user/${eMail}`).then(async response => {
		axios.get(`https://bce7b939410d46039ce06cb079b1e5dc.vfs.cloud9.us-east-2.amazonaws.com:8080/api/user/${eMail}`).then(async response => {
			if (response.data && response.data.password == password) {
				await this.setState({
					user: response.data,
					isLoggedIn: true
				})
				axios.get(`http://localhost:8080/api/users`).then(response => {
					this.setState({
						users: response.data.filter(user => user.id !== this.state.user.id)
					})
				})
			} else {
				console.error("Wrong data")
			}
		})
	}

	componentDidMount() {
		axios.get(`http://localhost:8080/api/subject`).then(response => {
			this.setState({
				subjects: response.data
			})
		})
	}

	render() {
		return (
			<div>
				{this.state.isLoggedIn ? (
					this.state.showGroupDetails ? (
						<GroupDocumentsView
							user={this.state.user}
							group={this.state.group}
							showGroupDetailsFalse={this.showGroupDetailsFalse}
							subjects={this.state.subjects}
							users={this.state.users}
						></GroupDocumentsView>
					) : (
						<DocumentsView
							user={this.state.user}
							showGroupDetailsTrue={this.showGroupDetailsTrue}
							subjects={this.state.subjects}
							users={this.state.users}
						></DocumentsView>
					)
				) : (
					<LoginView login={this.login}></LoginView>
				)}
			</div>
		)
	}
}

export default App
