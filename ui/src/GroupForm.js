import React from "react"
import axios from "axios"

class GroupForm extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			name: ""
		}
		this.change = this.change.bind(this)
		this.submit = this.submit.bind(this)
	}
	change(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async submit() {
		let group = {
			name: this.state.name,
			userId: this.props.user.id
		}
		await axios.post("http://localhost:8080/api/group", group)
		this.props.getGroups()
		this.setState({
			name: ""
		})
	}
	render() {
		return (
			<div className='col-sm-4 mb-3'>
				<form>
					<label>Name</label>
					<input
						value={this.state.name}
						className='form-control'
						type='text'
						name='name'
						onChange={this.change}
					/>
					<br />
					<button type='button' className='btn btn-xs btn-success' onClick={this.submit}>
						Add Group
					</button>
				</form>
			</div>
		)
	}
}

export default GroupForm
