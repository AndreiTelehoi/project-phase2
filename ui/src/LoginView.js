import React, { Component } from "react"

class LoginView extends Component {
	constructor(props) {
		super(props)

		this.state = {
			eMail: "",
			password: ""
		}

		this.change = this.change.bind(this)
		this.onOk = this.onOk.bind(this)
	}

	change(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	onOk() {
		this.props.login(this.state.eMail, this.state.password)
	}

	render() {
		return (
			<div>
				<div className='row justify-content-center'>
					<h2>Welcome!</h2>
					<br></br>
				</div>
				<div className='row justify-content-center'>
					<form>
						<input
							value={this.state.eMail}
							placeholder='eMail'
							className='form-control'
							type='text'
							name='eMail'
							onChange={this.change}
						/>
						<input
							value={this.state.password}
							placeholder='password'
							className='form-control'
							type='password'
							name='password'
							onChange={this.change}
						/>
						<button
							type='button'
							className='btn mt-1 btn-xs btn-success pull-right'
							onClick={this.onOk}
						>
							Login
						</button>
					</form>
				</div>
			</div>
		)
	}
}

export default LoginView
