import React from "react"
import axios from "axios"

class DocumentForm extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			name: "",
			content: "",
			subjectId: undefined,
			tag: ""
		}
		this.change = this.change.bind(this)
		this.submit = this.submit.bind(this)
	}
	change(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async submit() {
		let document = {
			name: this.state.name,
			data: new Date(),
			content: this.state.content,
			tag: this.state.tag,
			subjectId: parseInt(this.state.subjectId),
			userId: this.props.user.id
		}
		await axios.post("http://localhost:8080/api/document", document)
		this.props.getDocuments()
		this.setState({
			name: "",
			content: "",
			subjectId: undefined,
			tag: ""
		})
	}
	render() {
		return (
			<div className='col-sm-4 mb-3'>
				<form>
					<label>Name</label>
					<input
						value={this.state.name}
						className='form-control'
						type='text'
						name='name'
						onChange={this.change}
					/>
					<br />
					<label>Content</label>
					<input
						value={this.state.content}
						className='form-control'
						type='text'
						name='content'
						onChange={this.change}
					/>
					<br />
					<label>Tag</label>
					<input
						value={this.state.tag}
						className='form-control'
						type='text'
						name='tag'
						onChange={this.change}
					/>
					<label>Subject</label>
					<select
						value={this.state.subjectId}
						className='form-control'
						name='subjectId'
						onChange={this.change}
					>
						{this.props.subjects.map(subject => (
							<option value={subject.id}>{subject.name}</option>
						))}
					</select>
					<br />
					<button type='button' className='btn btn-xs btn-success' onClick={this.submit}>
						Add document
					</button>
				</form>
			</div>
		)
	}
}

export default DocumentForm
