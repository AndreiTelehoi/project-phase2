const express = require("express"),
	bodyParser = require("body-parser"),
	cors = require("cors")

const PORT = process.env.PORT || 8080
const router = require("./routes/routes")
const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use("/api", router)

app.listen(PORT, () => {
	console.log(`Server started at port ${PORT}...`)
})
