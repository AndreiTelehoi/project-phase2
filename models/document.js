const configuration = require("../config/configuration.json")
const Sequelize = require("sequelize")

const DB_NAME = configuration.database.database_name
const DB_USER = configuration.database.username
const DB_PASS = configuration.database.password

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
	dialect: "mysql"
})

sequelize
	.authenticate()
	.then(() => {
		console.log("Database connection success!")
	})
	.catch(err => {
		console.log(`Database connection error: ${err}`)
	})

class Document extends Sequelize.Model {}

Document.init(
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false
		},
		data: {
			type: Sequelize.DATE,
			allowNull: false
		},
		content: {
			type: Sequelize.TEXT,
			allowNull: false
		},
		subjectId: {
			type: Sequelize.INTEGER,
			allowNull: false
		},
		tag: {
			type: Sequelize.STRING,
			allowNull: true
		},
		userId: {
			type: Sequelize.INTEGER,
			allowNull: false
		}
	},
	{
		sequelize,
		modelName: "documents"
	}
)

// Document.sync({ force: true })

module.exports = {
	sequelize,
	Document
}
