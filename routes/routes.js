const express = require("express")
const router = express.Router()
const { getUser, getUsers } = require("../controller/user")
const {
	getDocument,
	getSharedDocument,
	createDocument,
	modifyDocument,
	deleteDocument
} = require("../controller/document")
const { getGroup, createGroup } = require("../controller/group")
const { getSubject } = require("../controller/subject")
const { createDocumentBind } = require("../controller/documentBind")
const {
	getGroupSharedDocuments,
	createDocumentGroupBind
} = require("../controller/documentGroupBind")
const { createGroupBind } = require("../controller/groupBind")

router.get("/user/:email", getUser)
router.get("/users", getUsers)
router.get("/document/:userId", getDocument)
router.get("/sharedDocument/:userId", getSharedDocument)
router.get("/groupSharedDocument/:groupId", getGroupSharedDocuments)
router.get("/group/:userId", getGroup)
router.get("/subject", getSubject)
router.post("/document", createDocument)
router.post("/documentGroupBind", createDocumentGroupBind)
router.post("/documentBind", createDocumentBind)
router.post("/group", createGroup)
router.post("/groupBind", createGroupBind)
router.put("/document/:documentId", modifyDocument)
router.delete("/document/:documentId", deleteDocument)

module.exports = router
