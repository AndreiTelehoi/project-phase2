const userService = require("./../service/user")

const getUser = async (req, res, next) => {
	try {
		const user = await userService.getByEmail(req.params.email)
		res.status(200).send(user)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const getUsers = async (req, res, next) => {
	try {
		const users = await userService.getUsers(req.params.email)
		res.status(200).send(users)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

module.exports = {
	getUser,
	getUsers
}
