const subjectService = require("./../service/subject")

const getSubject = async (req, res, next) => {
	try {
		const subjects = await subjectService.get()
		res.status(200).send(subjects)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

module.exports = {
	getSubject
}
