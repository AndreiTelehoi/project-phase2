const documentService = require("./../service/document")

const getDocument = async (req, res, next) => {
	// try {
		const documents = await documentService.getByUserId(req.params.userId)
	// 	res.status(200).send(documents)
	// } catch (err) {
	// 	res.status(500).send({
	// 		message: `Error occured: ${err.message}`
	// 	})
	// }
}

const getSharedDocument = async (req, res, next) => {
	try {
		const documents = await documentService.getShared(req.params.userId)
		res.status(200).send(documents)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createDocument = async (req, res, next) => {
	const document = req.body

	if (document.name && document.content && document.tag && document.subjectId && document.userId) {
		await documentService.create(document)
		res.status(201).send({
			message: "Created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

const modifyDocument = async (req, res, next) => {
	const document = req.body

	if (document && req.params.documentId) {
		await documentService.modify(req.params.documentId, document)
		res.status(201).send({
			message: "Modified"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

const deleteDocument = async (req, res, next) => {
	if (req.params.documentId) {
		await documentService.delete(req.params.documentId)
		res.status(201).send({
			message: "Deleted"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	getSharedDocument,
	getDocument,
	createDocument,
	modifyDocument,
	deleteDocument
}
