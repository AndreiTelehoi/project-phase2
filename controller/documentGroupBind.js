const documentGroupBindService = require("./../service/documentGroupBind")

const getGroupSharedDocuments = async (req, res, next) => {
	try {
		const documents = await documentGroupBindService.getGroupSharedDocuments(req.params.groupId)
		res.status(200).send(documents)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createDocumentGroupBind = async (req, res, next) => {
	const documentGroupBind = req.body

	if (documentGroupBind.documentId && documentGroupBind.groupId) {
		await documentGroupBindService.create(documentGroupBind)
		res.status(201).send({
			message: "Created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	getGroupSharedDocuments,
	createDocumentGroupBind
}
