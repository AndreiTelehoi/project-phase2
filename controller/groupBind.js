const groupBindService = require("./../service/groupBind")

const createGroupBind = async (req, res, next) => {
	const groupBind = req.body

	if (groupBind.userId && groupBind.groupId) {
		await groupBindService.create(groupBind)
		res.status(201).send({
			message: "Created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	createGroupBind
}
