const documentBindService = require("./../service/documentBind")

const createDocumentBind = async (req, res, next) => {
	const documentBind = req.body

	if (documentBind.userId && documentBind.documentId) {
		await documentBindService.create(documentBind)
		res.status(201).send({
			message: "Created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	createDocumentBind
}
