const groupService = require("./../service/group")

const getGroup = async (req, res, next) => {
	try {
		const groups = await groupService.getByUserId(req.params.userId)
		res.status(200).send(groups)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createGroup = async (req, res, next) => {
	const group = req.body

	if (group.name) {
		await groupService.create(group)
		res.status(201).send({
			message: "Created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	getGroup,
	createGroup
}
